Ansible Role: PostgreSQL
=========

Install PostgreSQL server on Debian/Ubuntu servers.

Requirements
------------
Can be installed on Debian/Ubuntu server.
This role requires root access (become: yes).

Role Variables
--------------
Available variables are listed below, along with default values (see defaults/main.yml).

```yaml
postgresql_locales:
  - 'en_US.UTF-8'
```
This variable used to generate the locales used by PostgreSQL databases.

Dependencies
------------

None.

Example Playbook
----------------

```yaml
- hosts: db
  roles:
      - ingniq.postgresql
```

License
-------

BSD

Author Information
------------------

This role was created by [Igor Filatov](https://gitlab.com/ingniq)
